from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from openapi_server.models.base_model import Model
from openapi_server import util


class QuotaCategoriesInnerItemsInner(Model):
    """NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).

    Do not edit the class manually.
    """

    def __init__(self, name=None, limit=None, used=None):  # noqa: E501
        """QuotaCategoriesInnerItemsInner - a model defined in OpenAPI

        :param name: The name of this QuotaCategoriesInnerItemsInner.  # noqa: E501
        :type name: str
        :param limit: The limit of this QuotaCategoriesInnerItemsInner.  # noqa: E501
        :type limit: str
        :param used: The used of this QuotaCategoriesInnerItemsInner.  # noqa: E501
        :type used: str
        """
        self.openapi_types = {
            'name': str,
            'limit': str,
            'used': str
        }

        self.attribute_map = {
            'name': 'name',
            'limit': 'limit',
            'used': 'used'
        }

        self._name = name
        self._limit = limit
        self._used = used

    @classmethod
    def from_dict(cls, dikt) -> 'QuotaCategoriesInnerItemsInner':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The Quota_categories_inner_items_inner of this QuotaCategoriesInnerItemsInner.  # noqa: E501
        :rtype: QuotaCategoriesInnerItemsInner
        """
        return util.deserialize_model(dikt, cls)

    @property
    def name(self) -> str:
        """Gets the name of this QuotaCategoriesInnerItemsInner.


        :return: The name of this QuotaCategoriesInnerItemsInner.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """Sets the name of this QuotaCategoriesInnerItemsInner.


        :param name: The name of this QuotaCategoriesInnerItemsInner.
        :type name: str
        """

        self._name = name

    @property
    def limit(self) -> str:
        """Gets the limit of this QuotaCategoriesInnerItemsInner.


        :return: The limit of this QuotaCategoriesInnerItemsInner.
        :rtype: str
        """
        return self._limit

    @limit.setter
    def limit(self, limit: str):
        """Sets the limit of this QuotaCategoriesInnerItemsInner.


        :param limit: The limit of this QuotaCategoriesInnerItemsInner.
        :type limit: str
        """

        self._limit = limit

    @property
    def used(self) -> str:
        """Gets the used of this QuotaCategoriesInnerItemsInner.


        :return: The used of this QuotaCategoriesInnerItemsInner.
        :rtype: str
        """
        return self._used

    @used.setter
    def used(self, used: str):
        """Sets the used of this QuotaCategoriesInnerItemsInner.


        :param used: The used of this QuotaCategoriesInnerItemsInner.
        :type used: str
        """

        self._used = used
