from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from openapi_server.models.base_model import Model
from openapi_server import util


class Health(Model):
    """NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).

    Do not edit the class manually.
    """

    def __init__(self, status=None, message=None):  # noqa: E501
        """Health - a model defined in OpenAPI

        :param status: The status of this Health.  # noqa: E501
        :type status: str
        :param message: The message of this Health.  # noqa: E501
        :type message: str
        """
        self.openapi_types = {
            'status': str,
            'message': str
        }

        self.attribute_map = {
            'status': 'status',
            'message': 'message'
        }

        self._status = status
        self._message = message

    @classmethod
    def from_dict(cls, dikt) -> 'Health':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The Health of this Health.  # noqa: E501
        :rtype: Health
        """
        return util.deserialize_model(dikt, cls)

    @property
    def status(self) -> str:
        """Gets the status of this Health.


        :return: The status of this Health.
        :rtype: str
        """
        return self._status

    @status.setter
    def status(self, status: str):
        """Sets the status of this Health.


        :param status: The status of this Health.
        :type status: str
        """
        allowed_values = ["OK", "ERROR"]  # noqa: E501
        if status not in allowed_values:
            raise ValueError(
                "Invalid value for `status` ({0}), must be one of {1}"
                .format(status, allowed_values)
            )

        self._status = status

    @property
    def message(self) -> str:
        """Gets the message of this Health.


        :return: The message of this Health.
        :rtype: str
        """
        return self._message

    @message.setter
    def message(self, message: str):
        """Sets the message of this Health.


        :param message: The message of this Health.
        :type message: str
        """

        self._message = message
