import connexion
from typing import Dict
from typing import Tuple
from typing import Union

from openapi_server.models.health import Health  # noqa: E501
from openapi_server import util


def healthcheck():  # noqa: E501
    """healthcheck

    Endpoint that can be used to know the health status of the API itself. # noqa: E501


    :rtype: Union[Health, Tuple[Health, int], Tuple[Health, int, Dict[str, str]]
    """
    return 'do some magic!'
