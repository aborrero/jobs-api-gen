import connexion
from typing import Dict
from typing import Tuple
from typing import Union

from openapi_server.models.defined_job import DefinedJob  # noqa: E501
from openapi_server.models.error import Error  # noqa: E501
from openapi_server.models.new_job import NewJob  # noqa: E501
from openapi_server import util


def create(new_job):  # noqa: E501
    """create

    Create a new job in this Toolforge tool account. # noqa: E501

    :param new_job: 
    :type new_job: dict | bytes

    :rtype: Union[DefinedJob, Tuple[DefinedJob, int], Tuple[DefinedJob, int, Dict[str, str]]
    """
    if connexion.request.is_json:
        new_job = NewJob.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def delete(id):  # noqa: E501
    """delete

    Stop and delete the specified job from this Toolforge tool account. # noqa: E501

    :param id: Job name.
    :type id: str

    :rtype: Union[DefinedJob, Tuple[DefinedJob, int], Tuple[DefinedJob, int, Dict[str, str]]
    """
    return 'do some magic!'


def flush():  # noqa: E501
    """flush

    Stop and delete all defined jobs from this Toolforge tool account. # noqa: E501


    :rtype: Union[None, Tuple[None, int], Tuple[None, int, Dict[str, str]]
    """
    return 'do some magic!'


def get_logs(id):  # noqa: E501
    """get_logs

    Get logs for a given job in this Toolforge tool account. # noqa: E501

    :param id: Job name.
    :type id: str

    :rtype: Union[str, Tuple[str, int], Tuple[str, int, Dict[str, str]]
    """
    return 'do some magic!'


def list():  # noqa: E501
    """list

    Returns the list of jobs defined for this Toolforge tool account. # noqa: E501


    :rtype: Union[List[DefinedJob], Tuple[List[DefinedJob], int], Tuple[List[DefinedJob], int, Dict[str, str]]
    """
    return 'do some magic!'


def restart(id):  # noqa: E501
    """restart

    Restart a given job. If the jobs is a cronjob, execute it right now. # noqa: E501

    :param id: Job name.
    :type id: str

    :rtype: Union[None, Tuple[None, int], Tuple[None, int, Dict[str, str]]
    """
    return 'do some magic!'


def show(id):  # noqa: E501
    """show

    Show information about a given job for this Toolforge tool account. # noqa: E501

    :param id: Job name.
    :type id: str

    :rtype: Union[DefinedJob, Tuple[DefinedJob, int], Tuple[DefinedJob, int, Dict[str, str]]
    """
    return 'do some magic!'
