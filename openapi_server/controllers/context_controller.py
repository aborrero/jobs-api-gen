import connexion
from typing import Dict
from typing import Tuple
from typing import Union

from openapi_server.models.error import Error  # noqa: E501
from openapi_server.models.images_inner import ImagesInner  # noqa: E501
from openapi_server.models.quota import Quota  # noqa: E501
from openapi_server import util


def images():  # noqa: E501
    """images

    Returns the list of available container images that you can use to create a new job. # noqa: E501


    :rtype: Union[List[ImagesInner], Tuple[List[ImagesInner], int], Tuple[List[ImagesInner], int, Dict[str, str]]
    """
    return 'do some magic!'


def quota():  # noqa: E501
    """quota

    Returns information on the tool account quotas in Toolforge. # noqa: E501


    :rtype: Union[Quota, Tuple[Quota, int], Tuple[Quota, int, Dict[str, str]]
    """
    return 'do some magic!'
