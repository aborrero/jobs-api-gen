import unittest

from flask import json

from openapi_server.models.error import Error  # noqa: E501
from openapi_server.models.images_inner import ImagesInner  # noqa: E501
from openapi_server.models.quota import Quota  # noqa: E501
from openapi_server.test import BaseTestCase


class TestContextController(BaseTestCase):
    """ContextController integration test stubs"""

    def test_images(self):
        """Test case for images

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/jobs/api/v1/images',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_quota(self):
        """Test case for quota

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/jobs/api/v1/quota',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
