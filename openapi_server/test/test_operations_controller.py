import unittest

from flask import json

from openapi_server.models.defined_job import DefinedJob  # noqa: E501
from openapi_server.models.error import Error  # noqa: E501
from openapi_server.models.new_job import NewJob  # noqa: E501
from openapi_server.test import BaseTestCase


class TestOperationsController(BaseTestCase):
    """OperationsController integration test stubs"""

    def test_create(self):
        """Test case for create

        
        """
        new_job = {"image":"tool-my-tool/tool-my-tool:latest","filelog":False,"cpu":"1","filelog_stdout":"logs/my-job.out","mount":"none","emails":"all","filelog_stderr":"logs/my-job.err","schedule":"@hourly","mem":"1G","continuous":False,"name":"my-job","cmd":"./some-command.sh --with-args","retry":0}
        headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/jobs/api/v1/jobs',
            method='POST',
            headers=headers,
            data=json.dumps(new_job),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete(self):
        """Test case for delete

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/jobs/api/v1/jobs/{id}'.format(id='id_example'),
            method='DELETE',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_flush(self):
        """Test case for flush

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/jobs/api/v1/jobs',
            method='DELETE',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_logs(self):
        """Test case for get_logs

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/jobs/api/v1/jobs/{id}/logs'.format(id='id_example'),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_list(self):
        """Test case for list

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/jobs/api/v1/jobs',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_restart(self):
        """Test case for restart

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/jobs/api/v1/jobs/{id}/restart'.format(id='id_example'),
            method='POST',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_show(self):
        """Test case for show

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/jobs/api/v1/jobs/{id}'.format(id='id_example'),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
