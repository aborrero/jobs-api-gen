import unittest

from flask import json

from openapi_server.models.health import Health  # noqa: E501
from openapi_server.test import BaseTestCase


class TestAPISystemController(BaseTestCase):
    """APISystemController integration test stubs"""

    def test_healthcheck(self):
        """Test case for healthcheck

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/jobs/api/v1/healthz',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
